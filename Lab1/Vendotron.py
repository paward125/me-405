# -*- coding: utf-8 -*-
"""
@file Vendotron.py
@brief This file contains a finite state machine for a vending machine
@details Takes in specific keystrokes for inputting payment and making product 
selections. Has two states: waiting for keystroke state and comparing payment to 
price state.
Source code can be found here: https://bitbucket.org/paward125/me-405/src/master/Lab1/Vendotron.py

@author: Patrick Ward
"""

"""
Initialize code to run FSM (not the init state). 
This code initializes the program, not the FSM
"""
import keyboard

## state variable that defines the current FSM state
state = 0

## Number of pennies in the vending machine
pens = 0

## Number of nickles in the vending machine
niks = 0

## Number of dimes in the vending machine
dims = 0

## Number of quarters in the vending machine
quar = 0

## Number of dollars in the vending machine
ones = 0

## Number of fives in the vending machine
fivs = 0

## Number of tens in the vending machine
tens = 0

## Number of twenties in the vending machine
twen = 0

## Price of current beverage selected
price = 0

## Value of most recent key press
pushed_key = None

def on_keypress (thing):
    """
    Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name

def getChange(price, payment):
    """
    @brief Computes Change for monetary transaction
    
    @details Computes change given set of bills/coins and returns denominations
    
    @param price The price of the item as an integer number of cents
    @param payment The amount of money available or recieved in the form of a 
    tuple
    
    @return If funds are sufficient returns a tuple of Bills/coins; If insufficient
    returns false
    """
    # Seperates the payment tuple out into pennies
    pennies = payment[0]
    pennies += (payment[1]*5)
    pennies += (payment[2]*10)
    pennies += (payment[3]*25)
    pennies += (payment[4]*100)
    pennies += (payment[5]*500)
    pennies += (payment[6]*1000)
    pennies += (payment[7]*2000)
    
    # If there is sufficient funds subtract price from payment
    if pennies >= price:
        
        change = pennies - price
        
        realChange = (0,0,0,0,0,0,0,0)
        
        
        twen = int(change/2000)
        change += -twen*2000
        tens = int(change/1000)
        change += -tens*1000
        fivs = int(change/500)
        change += -fivs*500
        ones = int(change/100)
        change += -ones*100
        quar = int(change/25)
        change += -quar*25
        dims = int(change/10)
        change += -dims*10
        niks = int(change/5)
        change += -niks*5
        pens = change
        
        realChange = (pens,niks,dims,quar,ones,fivs,tens,twen)
        return realChange
                        
    else:
        return None
    
    pass

def printWelcome():
    '''
    @brief Prints a welcome statement explaining how the vending machine works
    '''
    print('Welcome User. You will be able to choose a beverage to purchase at any time. To choose a beverage, press one of these four keys: c(Cuke), p(Popsi), s(Spryte), d(Dr. Pupper)')
    print('To insert payment press 0 for pennies, 1 for nickles, 2 for dimes, 3 for quarters, 4 for one dollar bills, 5 for 5 dollar bils, 6 for 10 dollar bills, and 7 for 20 dollar bills.')
    print('To eject inserted payment press e')
    pass

keyboard.on_press (on_keypress)  ########## Set callback

while True:
    try:
        if state == 0:
            printWelcome()
            state = 1
            price = 0
            
        elif state == 1:
            if pushed_key:
                if pushed_key == "c":
                    price = 400
                    state = 2
                    print('You have selected a Cuke with a price of $4')
                elif pushed_key == "p":
                    price = 375
                    state = 2
                    print('You have selected a Popsi with a price of $3.75')
                elif pushed_key == "d":
                    price = 425
                    state = 2
                    print('You have selected a Dr. Pupper with a price of $4.25')
                elif pushed_key == "s":
                    price = 300
                    state = 2
                    print('You have selected a Spryte with a price of $3')
                elif pushed_key == "0":
                    pens += 1
                    state = 2
                    print('You have inserted a pennie')
                elif pushed_key == "1":
                    niks += 1
                    state = 2
                    print('You have inserted a nickle')
                elif pushed_key == "2":
                    dims += 1
                    state = 2
                    print('You have inserted a dime')
                elif pushed_key == "3":
                    quar += 1
                    state = 2
                    print('You have inserted a quarter')
                elif pushed_key == "4":
                    ones += 1
                    state = 2
                    print('You have inserted a dollar')
                elif pushed_key == "5":
                    fivs += 1
                    state = 2
                    print('You have inserted five dollars')
                elif pushed_key == "6":
                    tens += 1
                    state = 2
                    print('You have inserted ten dollars')
                elif pushed_key == "7":
                    twen += 1
                    state = 2
                    print('You have inserted twenty dollars')
                elif pushed_key == "e":
                    print('You have ejected your payment')
                    price = 1
                    state = 0
                    pens = 0
                    niks = 0
                    dims = 0
                    quar = 0
                    ones = 0
                    fivs = 0
                    tens = 0
                    twen = 0
                else:
                    print('Please choose a valid key')
                    pass
                payment = (pens,niks,dims,quar,ones,fivs,tens,twen)
                print( ('Total inserted funds:\n'
                                ' %d pennies\n'
                                ' %d nickles\n'
                                ' %d dimes\n'
                                ' %d quarters\n'
                                ' %d dollars\n'
                                ' %d fives\n'
                                ' %d tens\n'
                                ' %d twenties\n') %payment)
            pushed_key = None 
            if price == 0:
                state = 1
            
        elif state == 2:
            realChange = getChange(price, payment)
            if realChange == None:
                print('You have entered insufficient funds for the selected beverage')
                print(('Total inserted funds:\n'
                                ' %d pennies\n'
                                ' %d nickles\n'
                                ' %d dimes\n'
                                ' %d quarters\n'
                                ' %d dollars\n'
                                ' %d fives\n'
                                ' %d tens\n'
                                ' %d twenties\n') %payment)
            else:
                print('Please retrieve your beverage')
                print(('Your remaining balance is:\n'
                                ' %d pennies\n'
                                ' %d nickles\n'
                                ' %d dimes\n'
                                ' %d quarters\n'
                                ' %d dollars\n'
                                ' %d fives\n'
                                ' %d tens\n'
                                ' %d twenties\n') %realChange)
                pens = realChange[0]
                niks = realChange[1]
                dims = realChange[2]
                quar = realChange[3]
                ones = realChange[4]
                fivs = realChange[5]
                tens = realChange[6]
                twen = realChange[7]
                price = 0
            state = 1
    # If Control-C is pressed, this is sensed separately from the keyboard
    # module; it generates an exception, and we break out of the loop
    except KeyboardInterrupt:
        
        break
    
print ("Control-C has been pressed, so it's time to exit.")
keyboard.unhook_all ()

        