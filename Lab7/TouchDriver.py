# -*- coding: utf-8 -*-
"""
@file TouchDriver.py
@brief A driver for a Touch Pad
@details This code receives pin information and touch panel dimensions.The driver
contains four methods: one method for x location, one method for y location, one
method for determining if the panel is being touched, and one for calling all three
methods. 

Source code found here: https://bitbucket.org/paward125/me-405/src/master/Lab7/TouchDriver.py

@author Patrick Ward
"""
import utime
from pyb import Pin
from pyb import ADC
from pyb import udelay

class TouchDriver:
    """
    @brief This class implements a touch pad driver
    @details This class contains init method that takes in the parameters for the 
    class and initiates the class as an object. In addition the class contains an X 
    method that finds the x position of the pressure on the touch pad, a Y method 
    that finds the y position of the pressure on the touch pad, and a Z method 
    that checks to see if the touch pad is being touched. Lastly, a total method
    is implemented to call all three methods and return them in a tuple.
    """

    def __init__ (self, xp, xm, yp, ym, length, width, xnot, ynot):
        """ 
        @brief Creates a touch bad driver 
        @details initializes variables for the class and creates conversion 
        factors for the the voltage outputs from the touchpad.
        @param xp A pyb.Pin object.
        @param xm A pyb.Pin object.
        @param yp A pyb.Pin object.
        @param ym A pyb.Pin object.
        @param length contains the x direction size of the touch panel.
        @param width contains the y direction size of the touch panel.
        @param xnot contains the center location in the x direction of the touch panel.
        @param ynot contains the center location in the y direction of the touch panel.
        """
        ## pyb.Pin object
        self.xp = xp
        
        ## pyb.Pin object
        self.xm = xm
        
        ## pyb.Pin object
        self.yp = yp
        
        ## pyb.Pin object
        self.ym = ym
        
        ## center location of the touch panel in the x-direction
        self.xnot = xnot
        
        ## center location of the touch panel in the y-direction
        self.ynot = ynot
        
        ## Conversion factor for the voltage output for the x-direction
        self.convx = length/3550
        
        ## Conversion factor for the voltage output for the y-direction
        self.convy = width/3220
        
    def X(self):
        """ 
        @brief Finds the x position of the pressure on the touch panel
        @return Method returns x possition in dimmensions matching the dimmensions 
        of class parameter length
        """
        # Setting up the pins in the configuration for measuring along x-axis
        self.xp.init(mode=Pin.OUT_PP, value=1)
        self.xm.init(mode=Pin.OUT_PP, value=0)
        self.yp.init(mode=Pin.IN)
        self.ym.init(mode=Pin.IN)
        udelay(5)
        
        ## An analog to digital converter object
        adc = ADC(self.ym)
        
        ## Contains the x location in units of length
        x = ((adc.read())*self.convx)-self.xnot
        
        return x
    
    def Y(self):
        """ 
        @brief Finds the y position of the pressure on the touch panel
        @return This method returns the y position in units of length
        """
        # Setting up the pins in the configuration for measuring along y-axis
        self.xp.init(mode=Pin.IN)
        self.xm.init(mode=Pin.IN)
        self.yp.init(mode=Pin.OUT_PP, value = 1)
        self.ym.init(mode=Pin.OUT_PP, value = 0)
        udelay(5)
        
        ## An analog to digital converter object
        adc = ADC(self.xm)
        
        ## Contains the y location in units of length
        y = ((adc.read())*self.convy)-self.ynot
        
        return y

    def Z(self):
        """
        @brief Determines if something is pressing on the touch panel or not
        @return This method returns True or False. If the touch pad is touched 
        it will return True and if not it will return False.
        """
        # Setting up the pins in the configuration for measuring contact
        self.xp.init(mode=Pin.IN)
        self.xm.init(mode=Pin.OUT_PP,value=0)
        self.yp.init(mode=Pin.OUT_PP, value=1)
        self.ym.init(mode=Pin.IN)
        udelay(5)
        
        ## An analog to digital converter object
        adc = ADC(self.ym)

        return (adc.read()<=3999)
        
    
    def total(self):
        """ 
        @brief Finds both x and y position and if the panel is being touched
        @return Returns a tuple containing the three other methods' outputs.
        """
        ## Stores all the location data
        tot = (self.X(),self.Y(),self.Z())
        return tot
        
        
        
if __name__ == '__main__':
    
    ## pyb.Pin object 
    xp = Pin.board.PA7
    
    ## pyb.Pin object
    xm = Pin.board.PA1
    
    ## pyb.Pin object
    yp = Pin.board.PA0
    
    ## pyb.Pin object
    ym = Pin.board.PA6   

    ## Stores a touch driver object passing in the pins and lengths
    touch = TouchDriver(xp, xm, yp, ym, 180,104,101,70)
        
    while True:
        utime.sleep(1)
        
        ## Contains the initial time stamp of calling the method total()
        start = utime.ticks_us()
        
        ## Contains the data from calling method touch()
        data = touch.total()
        #data = touch.X()
        #data = touch.Y()
        #data = touch.Z()
        
        ## Records the time it takes to run method total()
        time = utime.ticks_diff(utime.ticks_us(), start)
        
        print(str(data) + ' {:}'.format(time))
    
        
        