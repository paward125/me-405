# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 13:39:16 2021

@author: Patrick Ward
"""

import pyb

# xp = pyb.Pin.board.PA7
# xp.init(mode=pyb.Pin.OUT_PP, value=1)
# xm = pyb.Pin.board.PA1
# xm.init(mode=pyb.Pin.OUT_PP, value=0)
# yp = pyb.Pin.board.PA0
# yp.init(mode=pyb.Pin.IN)
# ym = pyb.Pin.board.PA6
# ym.init(mode=pyb.Pin.IN)
# adc = pyb.ADC(ym)

# xp = pyb.Pin.board.PA7
# xp.init(mode=pyb.Pin.IN)
# xm = pyb.Pin.board.PA1
# xm.init(mode=pyb.Pin.IN)
# yp = pyb.Pin.board.PA0
# yp.init(mode=pyb.Pin.OUT_PP, value=1)
# ym = pyb.Pin.board.PA6
# ym.init(mode=pyb.Pin.OUT_PP, value=0)
# adc = pyb.ADC(xm)

xp = pyb.Pin.board.PA7
xp.init(mode=pyb.Pin.IN)
xm = pyb.Pin.board.PA1
xm.init(mode=pyb.Pin.OUT_PP,value=0)
yp = pyb.Pin.board.PA0
yp.init(mode=pyb.Pin.OUT_PP, value=1)
ym = pyb.Pin.board.PA6
ym.init(mode=pyb.Pin.IN)
adc = pyb.ADC(ym)


while True:
    print(str(adc.read()))
