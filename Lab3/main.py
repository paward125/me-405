# -*- coding: utf-8 -*-
"""
@file main.py
@brief A program to be continuosly running on the NUCLEO to record voltage data
@details This code recieves data through the serial port. If the value of that
data is the decimal value for the ASCII characters g or G, then the program will
begin recording voltage data for the selected pin upon the user button being pressed
on the NUCLEO. The button may need to be pressed a couple times for the data to
line up correctly to record a step function. After data selection and recording the 
data is sent through the serial port.

Source code found here: https://bitbucket.org/paward125/me-405/src/master/Lab3/main.py

@author Patrick Ward
"""
import pyb
from pyb import UART
import array

## Contains a UART object
myuart = UART(2)

## Contains a Pin object
pin = pyb.Pin.board.PA0

## Contains an analog to digital converter object
adc = pyb.ADC(pin)

## Contains a Timer object
tim = pyb.Timer(2, freq=500000)

## Creates a buf for storing voltage data in
buf = array.array('H',(0 for index in range (1500)))

# This while loop checks for values in the serial port and if that value is equal to the ASCII value for G or g
while True:
    
    if myuart.any():                    # Checks for a character
        ## Stroes read characters
        char = myuart.readchar()        # Reads the character
        # If the read character is equal to the decimal value for the ASCII g or G data recording begins
        if char == 71 or 103:
            
            # This loop continues looping through to find the right start for the data collection
            while True:
                if adc.read() <= 1:                 # ensures that the button has been pressed
                    if adc.read() >=3:              # ensures that the button has been released to start data collection
                        adc.read_timed(buf,tim)     # Records voltage for the preallocated amount in the buf at the set frequency for the Timer
                        break
            # After data is collected it is sent through the serial using this for loop        
            for n in range(0,len(buf)):    
                myuart.write('{:}\r\n'.format(buf[n]))      # Writes an individual line of data
            myuart.write('dn')                              # Sends the end data to tell the computer it is done sending data
                    
                