# -*- coding: utf-8 -*-
"""
@file Frontend.py
@brief A user interface for recording voltage data from a NUCLEO
@details This code allows for a user to record the voltage data from a NUCLEO 
blue user push button. A g must be entered into the program to initiate and then
a while loop is used to process the data sent back through the serial port. This
processed data is used to create a plot and a csv file.

Source code found here: https://bitbucket.org/paward125/me-405/src/master/Lab3/Frontend.py

@author Patrick Ward
"""

import serial
import matplotlib.pyplot as plt
import numpy as np

## This is a serial object
ser = serial.Serial(port='COM3',baudrate=115273,timeout=20)
print('To begin data aquisition eneter a g when prompted.')
print('To ensure data aquisition works correctly please press the user button multiple times to allow for multiple data sets.')

## Stores the input value for data collection intiation
dataStart = (input('Press g to start:'))

# Sends the input value to the NUCLEO using the serial port
ser.write(str(dataStart).encode('ascii'))


## Contains time data
t = []

## Contains voltage data
Volt = []

## Acts as a counter and multiplier for creating the time array
n = 0

## Contains data read from the serial
line_string = []

# Receives all the data from the NUCLEO and processes it so that it can be useful for plotting
while True:
    line_string = ser.readline().decode()					# read a single line of text
    ## Contains modified data from the serial
    line_list = line_string.strip().split(',')		# strip any special characters and then split the string into wherever a comma appears;
    if line_list != ['dn']:                         # works until the while loop reads 'dn'
        try:
            ## Temporary voltage storage
            v = int(line_list[0])*3.3/4090          # Corrects the values to voltages using 3.3/4090 (used 4090 to give a little room at the top)
        except ValueError:
            pass
        else:
            t.append(n*2e-3)                            # adds time values based off of the frequency of the timer from main.py
            Volt.append(v)                              # Appends voltages
            n += 1                                      # Each loop through increases the counter
    else:
        break
ser.close()         # closes the serial port

plt.plot(t,Volt,'k.')       # plots the values using black dots
plt.ylabel('Voltage [V]')   # creates a y-label for the plot
plt.xlabel('Time [ms]')     # creates an x-label for the plot
plt.title('Voltage Step Function From A NUCLEO User Button')    # creates a title for the plot
plt.show()                  # Shows the plot in the spyder plot window

np.savetxt('VoltVsTim.csv',[t,Volt],delimiter=",")      # Saves the data in a CSV file


