# -*- coding: utf-8 -*-
"""
@file Lab02.py
@brief This file contains a reaction time test
@details A green light flashes at a time between 2 and 3 seconds. The user is intended to press the blue button
on the nucleo as fast as possible. To end the program the user hits control-c. When the program is 
terminated the average response time to the green light is reported. I worked on 
this lab with Ben Spin.
Source code can be found here: https://bitbucket.org/paward125/me-405/src/master/Lab2/Lab02.py

@author: Patrick Ward
"""
import urandom
import utime
import pyb

## This variable is the LED object
led = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

## This variable containss the Timer object
tim = pyb.Timer(2, prescaler=(79), period=0x7FFFFFFF)

## This variable contains the randomly generated timing of the green light
rand = 0

tim.counter()

## Stores the reaction time totals
react = 0

## Counts the number of times the button has been pressed
n = 0

## stores reference value for LED time stamp
reftime = 0

## contains the average reaction time
av = 0

def reactTim (which_pin):        # Create an interrupt service routine
    """
    @brief An interupt function that records reaction times.
    
    @details Adds time up consecutively and records number of button pushes so
    that the program can later calculate the average
    
    @param which_pin Probably contains a true but this parameter is not used in the function
    """
    global react
    global n
    react += tim.counter() - reftime
    n += 1
        
## Contains the interupt callback    
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             reactTim)                   # Interrupt service routine

# Below is the start-up message
print("Press the button once everytime the green light appears. Try your best to be as quick as possible.")
print("If you press the buttom too many times per green flash your average time will be penalized.")
print("Hit control-c to end the program and see your results.")
print("The blinking will begin 20 seconds after initiating the program to give you time to read this.")

# Sleeps to give user time to read
utime.sleep_ms(20000)

# While loop contains meat of program cycling through green pin being on
while True:
    try:
        rand = urandom.randrange(2000000,3000000,1)
        utime.sleep_us(rand)
        led.high()
        
        reftime = tim.counter()
        
        utime.sleep_ms(1000)
        led.low()
        
        # Creates a timeout 
        if tim.counter() >= 26000000 and n < 1:
            ## causes a Type Error
            typerror = 'one' + 2
        
    # If Control-C is pressed, this is sensed separately from the keyboard
    # module; it generates an exception, and we break out of the loop
    except KeyboardInterrupt:
        print ("Control-C has been pressed, so the average reaction time will be calculated.")
        if react == 0:
            print("Oops, you must be really drunk because no buttons were pressed.")
        else:
            av = str(react/n)
            print("Your average reaction time was:" + av + " microseconds")
        break
    # If any errors are caused the program assumes you are drunk 
    except:
        print("Oops, you must be really drunk because no buttons were pressed.")
        print("Try running the program again.")
        break
    
    
    
