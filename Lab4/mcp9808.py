# -*- coding: utf-8 -*-
"""
@file mcp9808.py
@brief A file containing a mcp9808 class
@details This code utilizes a function called access_bit to manage byte arrays
and convert them to bit arrays. This function allows for the class mcp9808 to 
convert the temperature data taken to degrees celcius and fahrenheit inside the
functions mcp9808.celcius and mcp9808.fahrenheit. The last method inportant to
this class is the mcp9808.check. It checks if a mcp9808 is connected and ready
to be used.

Source code found here: https://bitbucket.org/paward125/wardmurraylab/src/master/mcp9808.py

@author Patrick Ward and Matt Murray
"""

def access_bit(data, num):
    '''
    @brief Function takes in a byte array and selects 1 digit to return
    @details Uses floor division and modulus to select the digit and shift the
    byte array accordingly
    @param data is the bytearray being modified
    @param num is used for indexing
    @return This method returns the individual bit digits from a byte array
    '''
    ## Determines the digit being taken out
    base = int(num // 8)
    ## How much to shift the byte array
    shift = int(num % 8)
    return (data[base] & (1<<shift)) >> shift


class mcp9808:
    '''
    @brief An mcp9808 class
    @details This class contains an init method that runs on initiation.
    In addition it contains a check function for determining if the nucleo is connected
    to an mcp9808 and if that mcp9808 is ready to be accessed. The other two methods are
    celcius and fahrenheit which return the current temp of the mcp9808 in the 
    corresponding units.
    '''
    
    def __init__(self, I2C, address):
        '''
        @brief Creates an mcp9808 class object.
        @param I2C An I2C object
        @param address The mcp9808 address
        '''
        ## Contains the I2C object
        self.i2c = I2C
        
        ## Contains the mcp9808 address
        self.adi = address
        
        ## Contains an array that can be used to convert the data recorded into decimal temp values
        self.bit2temp = [2**4,2**5,2**6,2**7,0,0,0,0,2**(-4),2**(-3),2**(-2),2**(-1),2**(0),2**(1),2**(2),2**(3)]
    
    def check(self):
        '''
        @brief a method for checking if the Manufacturer ID register is correct
        @return The method Returns true if the mcp9808 is accessed and the correct
        Manufacturer ID register is correct. If any of that is not correct the method
        returns False.
        '''
        
        if self.i2c.mem_read(2,self.adi, 6) == b'\x00T':
            return True
        else:
            return False

        
    def celsius(self):
        '''
        @brief A method for taking temperature readings in celcius from the mcp9808
        @details uses the built in functionality of I2C from pyb and reads 2 bytes
        of data from the mcp9808 at register 5. The returned byte array is processed
        using the method access_bit and is compiled into a bit array in the form 
        of integers. This dot product between bit_array and bit2temp to find the
        magnitude of the temperature. Lastly bit 4 of bit_array is examined to determine
        the sign of the temperature.
        @return The method returns the signed decimal version of the temperature
        in degrees celsius.
        '''
        ## Temporarily holds temperature byte array
        data = self.i2c.mem_read(2,self.adi, 5)
        
        ## Holds the array of intergers representing the bits from data
        bit_array = [access_bit(data,i) for i in range(len(data)*8)]
        
        ## This is the variable used to store the decimal version of the temp data
        temp = 0
        
        # Basically completes a dot product of the two arrays bit_array and bit2temp
        for n in range(0,16):
            temp += bit_array[n]*self.bit2temp[n]
        
        # Checks the sign bit to determine if temp is negative or positive
        if bit_array[4] == '1':
            temp = temp*-1
        
        return temp
        
    def fahrenheit(self):
        '''
        @brief A method for taking temperature readings in celcius from the mcp9808
        @details uses the built in functionality of I2C from pyb and reads 2 bytes
        of data from the mcp9808 at register 5. The returned byte array is processed
        using the method access_bit and is compiled into a bit array in the form 
        of integers. This dot product between bit_array and bit2temp to find the
        magnitude of the temperature.Then bit 4 of bit_array is examined to determine
        the sign of the temperature. Lastly, the temperature is converted from 
        celsius to fahrenheit.
        @return This method returns the temperature as a signed decimal in degrees
        fahrenheit.
        '''
        
        ## Temporarily holds temperature byte array
        data = self.i2c.mem_read(2,self.adi, 5)
        
        ## Holds the array of intergers representing the bits from data
        bit_array = [access_bit(data,i) for i in range(len(data)*8)]
        
        ## This is the variable used to store the decimal version of the temp data
        temp = 0
        
        # Basically completes a dot product of the two arrays bit_array and bit2temp
        for n in range(0,16):
            temp += bit_array[n]*self.bit2temp[n]
            
        # Checks the sign bit to determine if temp is negative or positive    
        if bit_array[4] == '1':
            temp = temp*-1
        
        # Converts the temperature to fahrenheit
        temp = (temp*9/5) + 32
        
        return temp

# Below is test code to ensure the class worked properly. Samples temperature once
# every second.
if __name__ == "__main__":
    
    from pyb import I2C
    
    import utime
    
    ## Creating an I2C object
    i2c = I2C(1, I2C.MASTER)
    
    ## mcp9808 address
    adi = 24
    
    ## Creates an mcp9808 object
    therm = mcp9808(i2c, adi)
    
    # A while loop to continuously record temps every second.
    while True:
        utime.sleep(1)
        print(str(therm.celsius()))
    
