"""
@file main_Temp.py
@brief This program collects multiple sources of temperature data
@details This file is ran on the microcontroller and begins when the user resets the board. The
program collects temperature data from its own internal ADC that measures die tempature. it also collects temperature data
from the mcp9808 breakout board. it communicates with this board over I2C.
@author Matt Murray and Patrick Ward
@date 2/4/2021
"""

from mcp9808 import mcp9808
import pyb
from pyb import I2C
import utime
import array


## light object
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

## object for internal temp values
adc = pyb.ADCAll(12, 0x70000)

## object for I2C
i2c = I2C(1, I2C.MASTER)
i2c.init(I2C.MASTER)

##  @brief      I2C address value
#   @details    adi is the constant address value for our breakout board
adi = 24

## initialize I2C and let it settle
mcp = mcp9808(i2c, adi)
utime.sleep(2)



##  @brief      variabe to control when the program times out
#   @details    timeout is given in min. controls the number of data points to take
timeout = 480

##  @brief      variable to manage the current iteratio
#   @details    time will increase from 0 to timeout
time = 0

##  @brief      data storage array
#   @details    variable to hold the internal tempature data
internal_temp = array.array('d', [])

##  @brief      data storage array
#   @details    variable to hold the external tempature data (deg C)
sensor_C_val = array.array('d', [])

##  @brief      data storage array
#   @details    variable to hold the external tempature data (deg F)
sensor_F_val = array.array('d', [])

##  @brief      data storage array
#   @details    variable to hold the ticks in ms between each data point
time_val = array.array('L', [])


print(mcp.check())

utime.ticks_ms()

while time < timeout:
    ## work around to get good adc reading
    val = adc.read_vref()
    #collect data from sources
    #print(adc.read_core_temp())
    internal_temp.append(adc.read_core_temp())
    sensor_C_val.append(mcp.celsius())
    sensor_F_val.append(mcp.fahrenheit())
    #sleep
    utime.sleep(60)
    time_val.append(utime.ticks_ms())
    time = time + 1


with open('Lab4.csv', 'w') as file:
    ## indexing variable
    i = 0
    while i<len(sensor_C_val):
        file.write('{:}, {:}, {:}, {:}\r'.format(time_val[i]/60000, internal_temp[i], sensor_C_val[i], sensor_F_val[i]))
        i = i+1

#signal that data is done
blinko.high()





